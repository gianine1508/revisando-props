import React from "react";
import './App.css';
import {Title, SubTitle, Body, Footer} from './components/Title';

class App extends React.Component {
  render(){
    return (
      <div>
        <Title text="Aprendendo React" feeling="<3"></Title>
        <SubTitle text="Exercícios sobre props" feeling="<3"></SubTitle>
        <Body text="Prática leva a perfeição!" feeling="<3"></Body>
        <Footer text="Repetição para praticar!" feeling="<3"></Footer>
      </div>
    );
  }
}

export default App;
