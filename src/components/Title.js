import React from 'react';

class Title extends React.Component{
  render(){
    return(
      <h1>
        {this.props.text}
        <div>
          <TitleNote text="Repetir mais ainda!" feeling={this.props.feeling}></TitleNote>
        </div>
      </h1>
    );
  }
}

class TitleNote extends React.Component{
  render(){
    return(
      <div>{this.props.text} {this.props.feeling}</div>
    );
  }
}

class SubTitle extends React.Component{
  render(){
    return(
      <h2>
        {this.props.text}
        <div>
          <SubTitleNote text="Manda mais repetição!" feeling={this.props.feeling}></SubTitleNote>
        </div>
      </h2>
    );
  }
}

class SubTitleNote extends React.Component{
  render(){
    return(
      <div>{this.props.text} {this.props.feeling}</div>
    );
  }
}


class Body extends React.Component{
  render(){
    return(
      <div>
        {this.props.text}
        <div>
          <Note text="Vou continuar praticando!" feeling={this.props.feeling}></Note>
        </div>
      </div>
    );
  }
}

class Note extends React.Component{
  render(){
    return(
      <div>{this.props.text} {this.props.feeling}</div>
    );
  }
}

class Footer extends React.Component{
  render(){
    return(
      <div>
        {this.props.text}
        <div>
          <FooterNote text="Repetir mais!" feeling={this.props.feeling}></FooterNote>
        </div>
      </div>
    );
  }
}

class FooterNote extends React.Component{
  render(){
    return(
      <div>{this.props.text} {this.props.feeling}</div>
    );
  }
}

export { Title, SubTitle, Body, Footer};

//export default Title;
//export class SubTitle;
//export default Body;
//export class Footer;